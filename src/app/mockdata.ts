import { Measurement } from "../models/measurement.interface";

export const mockdata: Measurement[] = [
  {
    id: "f4893524-b231-46de-91c7-91c7fa4ba63f",
    value: 10,
    name:
      "ACETAMINOPHEN, CHLORPHENIRAMINE MALEATE AND PHENYLEPHRINE HYDROCHLORIDE",
    scientist: "Liana Crilly",
    time: "2020-06-28T09:25:42Z"
  },
  {
    id: "e9aa0aa7-662a-4797-afee-2fb3ce8eb888",
    value: 54,
    name: "repaglinide and metformin hydrochloride",
    scientist: "Katrine Baraja",
    time: "2019-10-11T16:57:58Z"
  },
  {
    id: "31cfce6c-62d0-4b52-b094-8e26f319e0c5",
    value: 29,
    name: "somatropin (rDNA origin)",
    scientist: "Bird Kretchmer",
    time: "2019-07-18T03:32:54Z"
  },
  {
    id: "6f7c21d0-8bde-4dc3-aa39-6480e9ffca80",
    value: 73,
    name: "milnacipran hydrochloride",
    scientist: "Antonius Flacke",
    time: "2019-10-24T15:49:19Z"
  },
  {
    id: "fd7abadd-9325-4518-8de1-6c6bfdb86064",
    value: 26,
    name: "Prednisone",
    scientist: "Selig Coakley",
    time: "2020-04-26T15:37:42Z"
  },
  {
    id: "552c0bdb-10af-4a62-9211-30c9c6795463",
    value: 6,
    name: "simvastatin",
    scientist: "Marie-ann Kleinsinger",
    time: "2020-01-09T06:27:54Z"
  },
  {
    id: "b29d681a-8048-479a-88cb-7b5dd4f8aedf",
    value: 68,
    name: "VANCOMYCIN HYDROCHLORIDE",
    scientist: "Fredra Kruschev",
    time: "2020-01-02T21:21:47Z"
  },
  {
    id: "2834648e-57e3-4e06-a7ed-dc8127ed6e35",
    value: 82,
    name: "Octinoxate and Zinc Oxide",
    scientist: "Cullen Shea",
    time: "2019-10-21T09:29:48Z"
  },
  {
    id: "4948a9fd-5382-4c23-a0cd-50a7e106cff2",
    value: 73,
    name: "DOCUSATE SODIUM",
    scientist: "Loni Walley",
    time: "2020-02-14T12:05:57Z"
  },
  {
    id: "9ef2bbff-905b-4553-87d4-d2f366328f43",
    value: 58,
    name: "Red Delicious Apple",
    scientist: "Koressa Drayton",
    time: "2020-02-05T18:27:37Z"
  },
  {
    id: "03e74cea-1253-4032-a86b-ef067e7bbd9d",
    value: 61,
    name: "Diphenhydramine HCl",
    scientist: "Ulrica Aries",
    time: "2020-01-07T23:10:10Z"
  },
  {
    id: "cb356654-481d-42c5-b752-a7d2808ef320",
    value: 78,
    name: "diltiazem hydrochloride",
    scientist: "Rollie Cairney",
    time: "2020-02-17T10:08:17Z"
  },
  {
    id: "6ee45bed-b4e0-4fd1-9847-3d5fefdbb32a",
    value: 34,
    name: "DEXTROSE MONOHYDRATE",
    scientist: "Marys Goring",
    time: "2019-09-04T09:51:34Z"
  },
  {
    id: "04a5ae10-cae1-430f-ba93-13af2fa311ef",
    value: 4,
    name:
      "Dextromethorphan Hydrobromide, Guaifenesin, Phenylephrine Hydrochloride",
    scientist: "Evaleen Huws",
    time: "2019-09-15T08:24:17Z"
  },
  {
    id: "2eba842f-99ca-4a2c-a6ec-ed1bf4ceb25e",
    value: 74,
    name:
      "Anacardium orientale, Antimon.crud., Arg. nit., Berber. vulg., Bryonia, Chelidonium majus, Digitalis, Graphites, Humulus,Iris versicolor, Kali carb., Lycopodium, Nat. carb.,Nat. sulphuricum, Nux vom., Pulsatilla, Rhus. toxicodendron, Scutellaria lateriflora, Sepia,Stramonium, Chamomilla, Passiflora, Valeriana.",
    scientist: "Cammy Tiffany",
    time: "2020-03-28T03:26:20Z"
  },
  {
    id: "bcd9c9c1-1e43-42ad-b53d-2c7986bad6ee",
    value: 33,
    name: "ofloxacin",
    scientist: "Orville Kasperski",
    time: "2019-10-24T21:13:37Z"
  },
  {
    id: "2da265b3-77d3-4eaa-90b5-2802bde89d2f",
    value: 44,
    name: "Amoxicillin and Clavulanate Potassium",
    scientist: "Wendy Wagge",
    time: "2020-05-18T04:37:36Z"
  },
  {
    id: "37e7f38d-f4cf-40d6-af27-501812c1d645",
    value: 1,
    name: "Oxytocin",
    scientist: "Netta Oswick",
    time: "2019-08-03T17:38:43Z"
  },
  {
    id: "ce7fcd6b-ae87-4612-849a-cefa5d4dd856",
    value: 68,
    name: "Cottonwood Eastern Common",
    scientist: "Oswald Frigot",
    time: "2020-06-06T23:15:06Z"
  },
  {
    id: "3d7b194a-7bec-46d1-9616-2d9eade60738",
    value: 79,
    name: "Dicyclomine Hydrochloride",
    scientist: "Gilburt Parsley",
    time: "2019-11-28T01:27:02Z"
  },
  {
    id: "714d9702-ee1c-46b4-91e0-551361756131",
    value: 70,
    name: "Methocarbamol",
    scientist: "Powell Kielt",
    time: "2019-10-18T08:33:23Z"
  },
  {
    id: "1feb7d6c-684e-42a8-a6f0-5bed932ac48e",
    value: 2,
    name: "naproxen sodium",
    scientist: "Fergus Kardos-Stowe",
    time: "2019-11-04T09:09:33Z"
  },
  {
    id: "c565aebf-2509-439d-8717-630dae66bb7e",
    value: 34,
    name: "citalopram hydrobromide",
    scientist: "Carolee Kleinert",
    time: "2020-02-28T22:35:49Z"
  },
  {
    id: "9a6094eb-9f19-4749-92e3-854cbd16fd1c",
    value: 39,
    name: "SODIUM BICARBONATE",
    scientist: "Robert Kender",
    time: "2019-08-16T04:56:13Z"
  },
  {
    id: "5f5e6ee8-14cb-4218-acd9-5001511249f8",
    value: 39,
    name: "Sodium Fluoride",
    scientist: "Tabina Strelitzki",
    time: "2019-12-28T18:47:32Z"
  },
  {
    id: "74e4c127-4828-4270-86ed-8d77f0bf8301",
    value: 92,
    name:
      "Elaps corallinus, Agnus, Angelica sinensis radix, Arsenicum alb., Bryonia, Cantharis, Caulophyllum, Cimicifuga, Cinchona, Crotalus horridus, Damiana, Glonoinum, Helonias dioica, Ignatia, Kali carb., Lachesis, Naja, Nux vom., Phosphorus, Pulsatilla, Salix nigra, Sanguinaria, Sepia, Echinacea, Valeriana",
    scientist: "Nevile Flecknoe",
    time: "2019-11-21T13:16:55Z"
  },
  {
    id: "d9803890-be6a-4e7e-8c1d-d5aeb81a2964",
    value: 7,
    name: "Nortriptyline Hydrochloride",
    scientist: "Yancy Chamberlin",
    time: "2019-09-23T20:03:17Z"
  },
  {
    id: "2d872912-c17f-46a6-86d3-93722da26be4",
    value: 72,
    name: "darunavir",
    scientist: "Welbie Whelpton",
    time: "2020-03-31T04:37:42Z"
  },
  {
    id: "4b2649c3-761d-4c61-8884-9df1df26f76e",
    value: 40,
    name: "Egg Yolk",
    scientist: "Valdemar Wintle",
    time: "2019-12-08T16:35:25Z"
  },
  {
    id: "a4d26a0b-cdc7-4630-afe3-015027b6b019",
    value: 12,
    name: "EUCALYPTOL, MENTHOL, METHYL SALICYLATE, THYMOL",
    scientist: "Seka Saunder",
    time: "2020-03-23T00:49:28Z"
  },
  {
    id: "e24b3009-8dc8-4be0-aadf-74deb8e27192",
    value: 1,
    name: "PAIN RELIEVING PLASTER",
    scientist: "Austen Hunton",
    time: "2019-07-11T01:33:15Z"
  },
  {
    id: "48d32614-a417-43cf-b837-c8c0d8c3e861",
    value: 46,
    name: "Guiafenesin, Phenylephrine HCl",
    scientist: "Tobit Blaszczak",
    time: "2020-03-15T20:07:30Z"
  },
  {
    id: "d3bcd3ba-7349-4ec7-b972-4f97a2c6b46d",
    value: 76,
    name: "Pramipexole Dihydrochloride",
    scientist: "Arlan Daniello",
    time: "2020-05-20T14:41:59Z"
  },
  {
    id: "073d6e78-b198-4a16-bb6e-9bfb0540aa71",
    value: 15,
    name: "alcohol",
    scientist: "Arnaldo MacInnes",
    time: "2019-07-19T12:29:34Z"
  },
  {
    id: "69aa2791-d4c0-4fe9-8dfb-9c098679efa0",
    value: 89,
    name: "imipenem and cilastatin sodium",
    scientist: "Batholomew Yarmouth",
    time: "2019-08-09T06:12:03Z"
  },
  {
    id: "803a7e4f-f80d-4e4f-96b6-548369af772c",
    value: 75,
    name: "SALICYLIC ACID",
    scientist: "Teodoro Blazewicz",
    time: "2020-06-02T04:42:58Z"
  },
  {
    id: "67572c15-2ccc-48c9-bb58-6e53a13810ef",
    value: 72,
    name:
      "Aurum metallicum, Cocculus indicus, Colocynthis, Gelsemium sempervirens, Hyoscyamus niger, Ignatia amara, Lachesis mutus, Natrum muriaticum, Phosphoricum acidum, Pulsatilla, Staphysagria",
    scientist: "Janice Rosthorn",
    time: "2020-05-25T04:15:54Z"
  },
  {
    id: "040b3490-15bb-49f0-ae3e-00ce4f9ddfbe",
    value: 60,
    name: "Fucus vesiculosus,",
    scientist: "Penni McGourty",
    time: "2019-12-01T05:37:48Z"
  },
  {
    id: "12234d5c-9708-4953-ab0b-e6973ee28202",
    value: 22,
    name: "Ondansetron",
    scientist: "Alessandro Welds",
    time: "2020-02-14T11:00:31Z"
  },
  {
    id: "f366a4f6-e62e-4ffa-aaee-ef85bcf5c264",
    value: 8,
    name: "Levothyroxine Sodium",
    scientist: "Eulalie Simester",
    time: "2020-02-01T15:00:22Z"
  },
  {
    id: "60a1ab57-ebeb-45a3-ba2b-34d6a71e7c8b",
    value: 57,
    name: "Warfarin Sodium",
    scientist: "Harriot Spowage",
    time: "2020-05-19T15:34:34Z"
  },
  {
    id: "2e0c7a50-05f2-4d06-81e2-86555a88d375",
    value: 35,
    name: "fibrinogen human and thrombin human",
    scientist: "Margy Rigard",
    time: "2020-04-06T10:40:37Z"
  },
  {
    id: "74281669-7bf6-4f92-b5ff-99bea931e31c",
    value: 2,
    name: "NITROUS OXIDE",
    scientist: "Alaster Pinsent",
    time: "2019-06-30T08:58:38Z"
  },
  {
    id: "27724dc3-2a44-4de5-9757-9aa35a06aa37",
    value: 21,
    name: "Clotrimazole",
    scientist: "Rozalie Snoddon",
    time: "2019-08-12T20:41:28Z"
  },
  {
    id: "6870fc9d-7cdc-437b-8f02-bf3b5d189ddc",
    value: 23,
    name: "GUAIFENESIN",
    scientist: "Alfy Trowill",
    time: "2019-08-30T11:03:33Z"
  },
  {
    id: "fa8e5f1a-38dc-4b40-ad41-b3c015f3d302",
    value: 64,
    name: "CLONIDINE HYDROCHLORIDE",
    scientist: "Cele Janaud",
    time: "2020-03-20T05:19:06Z"
  },
  {
    id: "abf94ae0-9456-4a11-aad6-666f174a7b40",
    value: 41,
    name: "metformin hydrochloride",
    scientist: "Elaine Marryatt",
    time: "2019-09-15T04:07:54Z"
  },
  {
    id: "75216565-c9dd-4451-a77c-e676d5ac3a17",
    value: 11,
    name: "atorvastatin calcium",
    scientist: "Shawna Wisedale",
    time: "2020-01-05T02:57:20Z"
  },
  {
    id: "fe762418-6f82-4ac0-805a-3850fd96a27a",
    value: 80,
    name: "Ibuprofen",
    scientist: "Farrell Grisard",
    time: "2019-09-25T12:17:21Z"
  },
  {
    id: "e5b8abb0-5819-457b-b0aa-664d54aedb30",
    value: 39,
    name: "stannous fluoride",
    scientist: "Abbi Evins",
    time: "2020-04-26T22:52:21Z"
  },
  {
    id: "286d77b4-dbb8-4a3c-80ec-4000dcbea66b",
    value: 2,
    name: "Ibuprofen",
    scientist: "Manuel Gress",
    time: "2020-01-20T10:54:41Z"
  },
  {
    id: "0d85f33d-c488-4880-9acd-94a724c56580",
    value: 7,
    name: "pantoprazoel sodium",
    scientist: "Lonni Husselbee",
    time: "2020-05-28T00:59:24Z"
  },
  {
    id: "251ab8f0-3ff0-4177-9051-e0f3a61f552a",
    value: 58,
    name: "Benzocaine",
    scientist: "Herman Saw",
    time: "2019-10-01T04:47:20Z"
  },
  {
    id: "0aaedf5e-a5b3-43b5-a1be-3f401363f8bf",
    value: 92,
    name:
      "Arnica montana, Caladium seguinum, Carduus marianus, Damiana, Galium aparine, Glandula suprarenalis suis, Hepar suis, Korean ginseng, Lactuca virosa,",
    scientist: "Cookie Forgie",
    time: "2020-03-22T18:32:57Z"
  },
  {
    id: "4dcf3268-3b77-41c7-8a1e-6e31f09b3958",
    value: 3,
    name: "Lansoprazole",
    scientist: "Ranna Powder",
    time: "2020-04-22T22:54:32Z"
  },
  {
    id: "05cfcb83-e79b-4acf-b29d-6229398c0224",
    value: 74,
    name: "Zinc Oxide, Octinoxate, Octisalate",
    scientist: "Allistir MacShane",
    time: "2020-04-15T08:18:42Z"
  },
  {
    id: "ce0d7d86-f52c-461e-ac1a-63f28414fe16",
    value: 0,
    name: "GEMCITABINE HYDROCHLORIDE",
    scientist: "Editha Kinzett",
    time: "2019-10-23T05:03:51Z"
  },
  {
    id: "40ea6c32-c4c6-4997-b20d-a95b68705a56",
    value: 65,
    name: "Acetaminophen, Chlorpheniramine Maleate",
    scientist: "Marje Maffetti",
    time: "2019-11-18T12:18:35Z"
  },
  {
    id: "49aa5d41-9b28-4bf8-8dd6-36e2fd52857b",
    value: 77,
    name: "Estazolam",
    scientist: "Shelbi Marquand",
    time: "2020-03-27T21:35:10Z"
  },
  {
    id: "dff49a3e-0280-4390-8627-8ceb59e10fa9",
    value: 79,
    name:
      "OCTINOXATE, OCTISALATE, TITANIUM DIOXIDE, AVOBENZONE, ENZACAMENE, NIACINAMIDE, ADENOSINE",
    scientist: "Dynah Chapelhow",
    time: "2020-05-11T11:50:15Z"
  },
  {
    id: "cfe3a80b-a296-4ebe-bfc8-730ee95d08ac",
    value: 53,
    name:
      "Calcarea Carbonica, Chininum Sulphuricum, Kali Carbonicum, Lycopodium Clavatum, Salicylicum Acidum",
    scientist: "Timmie Skirvane",
    time: "2019-08-03T02:21:11Z"
  },
  {
    id: "30c5b3a2-6cd7-4221-855a-019c20cb26c4",
    value: 64,
    name: "Antipyrine and Benzocaine",
    scientist: "Percival Summerscales",
    time: "2020-05-27T15:49:06Z"
  },
  {
    id: "79b274e0-06fa-4b60-b9e5-774d9fbfd820",
    value: 47,
    name: "hydrocortisone",
    scientist: "Laure Ezele",
    time: "2019-11-02T05:11:10Z"
  },
  {
    id: "f446b691-9804-40b2-8d06-7641e6a6be18",
    value: 77,
    name: "oxycodone hydrochloride",
    scientist: "Gerrie Micheu",
    time: "2019-08-26T02:35:09Z"
  },
  {
    id: "3d9423c7-f84c-4420-90f6-d843c1b17198",
    value: 92,
    name: "ETHYL ALCOHOL",
    scientist: "Rhett Tapin",
    time: "2019-12-05T00:57:25Z"
  },
  {
    id: "f40aa9af-304d-42d6-a827-8f3caa0beb2a",
    value: 72,
    name: "methylphenidate hydrochloride",
    scientist: "Kiele Gunderson",
    time: "2019-07-30T15:43:35Z"
  },
  {
    id: "19d730e8-3dad-4d81-adb9-c70e0dbb1b32",
    value: 29,
    name:
      "Aconitum nap., Ambra, Anacardium orientale, Arg.nit., Asafoetida, Bryonia, Chamomilla, Cimicifuga, Coffea cruda, Cypripedium, Gelsemium, Hyoscyamus, Hypericum, Ignatia, Kali phos., Nux vom., Sambucus nig., Strychninum, Theridion, Valeriana, Passiflora",
    scientist: "Corrinne Vermer",
    time: "2019-12-24T02:03:59Z"
  },
  {
    id: "9517771d-bf1c-463e-aa73-e334c3555c97",
    value: 26,
    name:
      "Kali phosphoricum, Phosphoricum acidum, Sepia, Sulphus Calcarea carbonica, Silicea,",
    scientist: "Charlotte Hathwood",
    time: "2019-10-07T02:42:22Z"
  },
  {
    id: "858fc969-a72a-4028-954e-67e067089227",
    value: 6,
    name: "Hypericum perforatum, Ledum palustre, Latrodectus mactans,",
    scientist: "Adore Bramsen",
    time: "2019-07-27T05:29:13Z"
  },
  {
    id: "90de94c0-7252-4824-9f3a-6923364a3d36",
    value: 52,
    name: "DL-Camphor, L-Menthol, Methyl Salicylate patch",
    scientist: "Ryley Counsell",
    time: "2019-10-09T02:53:51Z"
  },
  {
    id: "d00457f4-3712-406d-89c6-62e5f00928e8",
    value: 44,
    name: "Sodium Monofluorophosphate",
    scientist: "Holly Aucott",
    time: "2020-03-10T15:46:42Z"
  },
  {
    id: "d219f600-a1b7-4243-901c-2841aaab10c5",
    value: 70,
    name: "Glipizide and Metformin HCl",
    scientist: "Andras Wiley",
    time: "2019-12-06T05:24:35Z"
  },
  {
    id: "9167f2b1-6554-40ed-9134-59aaa5f5992b",
    value: 14,
    name: "Brome Grass",
    scientist: "Alys Napolione",
    time: "2019-08-12T08:32:20Z"
  },
  {
    id: "2169be83-5626-4511-bc39-510816a103b3",
    value: 94,
    name: "Sennosides",
    scientist: "Ruperto Nunnerley",
    time: "2020-02-15T09:01:25Z"
  },
  {
    id: "66bbc14c-559c-41a0-8b61-5c57d50471e7",
    value: 2,
    name: "ETHYL ALCOHOL",
    scientist: "Binni Neild",
    time: "2019-07-18T10:31:51Z"
  },
  {
    id: "53c4dd4b-ead0-431b-9737-0442b875d8e6",
    value: 24,
    name: "Tolnaftate",
    scientist: "Idell Pyott",
    time: "2019-08-10T16:56:59Z"
  },
  {
    id: "619f668e-9c0b-4d96-9258-ca3ed0029232",
    value: 78,
    name: "Azelastine Hydrochloride",
    scientist: "Edgar Leese",
    time: "2019-09-14T18:06:16Z"
  },
  {
    id: "41e17493-2c0f-4022-be4e-58e396c6fd2f",
    value: 59,
    name: "Titanium Dioxide and Zinc Oxide",
    scientist: "Murvyn Swalwell",
    time: "2019-09-08T10:43:57Z"
  },
  {
    id: "253af296-bf2b-47ed-a4b1-b7d508096b23",
    value: 40,
    name: "Aluminum Zirconium Tetrachlorohydrex GLY",
    scientist: "Flor Follet",
    time: "2020-04-06T17:37:04Z"
  },
  {
    id: "585dfed6-d978-44c8-9131-5e1f5534e719",
    value: 45,
    name: "Imipramine Hydrochloride",
    scientist: "Farica Tagg",
    time: "2019-09-11T12:29:16Z"
  },
  {
    id: "f3c246e7-3718-4693-add8-b15e75d7cd9c",
    value: 50,
    name: "Digoxin",
    scientist: "Rea Swabey",
    time: "2020-03-03T22:30:54Z"
  },
  {
    id: "9ec25d2a-80b8-4dd1-9446-d8fe3f5a6c31",
    value: 13,
    name: "Cefepime Hydrochloride",
    scientist: "Dyan Heynen",
    time: "2020-01-23T09:58:34Z"
  },
  {
    id: "1e97d288-420b-4335-ace8-2c3d0202a093",
    value: 23,
    name: "Acetaminophen",
    scientist: "Bessie Crunkhorn",
    time: "2019-11-02T11:48:10Z"
  },
  {
    id: "62d339f7-3611-4cdf-9102-bd991319197c",
    value: 13,
    name: "SODIUM FLUORIDE",
    scientist: "Sheppard Melmoth",
    time: "2020-01-31T19:48:02Z"
  },
  {
    id: "bbfb08b7-9f33-45e8-ba0f-da2f078689b1",
    value: 76,
    name: "Benzalkonium chloride",
    scientist: "Jemimah Heminsley",
    time: "2019-10-02T22:47:50Z"
  },
  {
    id: "935334dd-2e38-4d61-97b8-2323ec83afeb",
    value: 73,
    name: "Lorazepam",
    scientist: "Marven Sleit",
    time: "2020-04-16T01:52:57Z"
  },
  {
    id: "49ec0674-691a-47b8-8b78-29103da4892c",
    value: 49,
    name: "nystatin and triamcinolone acetonide",
    scientist: "Catha Guille",
    time: "2019-11-04T01:32:21Z"
  },
  {
    id: "c0add9de-3f8d-4964-9365-3abf655cf918",
    value: 31,
    name: "Sugar Maple",
    scientist: "Dory Blaxley",
    time: "2020-05-17T10:28:55Z"
  },
  {
    id: "b907f7b8-51cd-479d-9369-5526a424940a",
    value: 33,
    name:
      "HELIANTHEMUM NUMMULARIUM FLOWER, CLEMATIS VITALBA FLOWER, IMPATIENS GLANDULIFERA FLOWER, PRUNUS CERASIFERA FLOWER, and ORNITHOGALUM UMBELLATUM",
    scientist: "Sandye Bruineman",
    time: "2020-03-19T10:22:55Z"
  },
  {
    id: "a64dd988-fa17-46b8-9b7f-67e0e22a7427",
    value: 53,
    name: "Sodium Fluoride",
    scientist: "Kippy O' Donohoe",
    time: "2019-12-15T21:22:59Z"
  },
  {
    id: "10a38307-c7ac-4d6c-a579-0c6cf438d698",
    value: 92,
    name: "Loratadine",
    scientist: "Margareta Darey",
    time: "2020-05-29T21:41:23Z"
  },
  {
    id: "65883fa3-0f9a-4a92-b4b5-531c2dd6cb47",
    value: 35,
    name: "Betamethasone Dipropionate",
    scientist: "Andrus Truluck",
    time: "2020-02-21T16:54:16Z"
  },
  {
    id: "9b314944-cee3-41ab-bf71-7f4890850ad4",
    value: 44,
    name: "Octinoxate and Titanium Dioxide",
    scientist: "Dniren Zahor",
    time: "2019-08-13T00:26:03Z"
  },
  {
    id: "a2a7e073-afca-4c7a-bf72-f88939616411",
    value: 51,
    name: "Aspergillus repens",
    scientist: "Harwell Barti",
    time: "2020-06-07T05:58:00Z"
  },
  {
    id: "f0b92afa-50ca-4a6d-9a23-d1864f50037e",
    value: 70,
    name: "Povidone-Iodine Sponge",
    scientist: "Ichabod Colebrook",
    time: "2019-12-10T02:15:43Z"
  },
  {
    id: "dd0449fd-07ee-4ef4-b693-23d972704681",
    value: 64,
    name: "DEXTROSE MONOHYDRATE, SODIUM CHLORIDE, and POTASSIUM CHLORIDE",
    scientist: "Otho Ravilious",
    time: "2019-10-18T04:55:21Z"
  },
  {
    id: "30eed497-1293-4ffd-94b5-cdf80de42fea",
    value: 70,
    name: "repaglinide and metformin hydrochloride",
    scientist: "Barnie Kilborn",
    time: "2020-06-05T02:16:10Z"
  },
  {
    id: "24c4ee36-935e-4a66-9e3c-e0dd7c6dca8d",
    value: 72,
    name: "amoxicillin and clavulanate potassium",
    scientist: "Far Baggot",
    time: "2020-01-16T01:49:36Z"
  },
  {
    id: "da261dd9-d9b4-4a24-a8e5-04bbe239f5e8",
    value: 71,
    name: "citalopram hydrobromide",
    scientist: "Sileas Nertney",
    time: "2019-07-04T01:19:01Z"
  },
  {
    id: "b35cd8d7-36d2-4287-95b5-16b0d6e0219a",
    value: 100,
    name: "MORPHINE SULFATE",
    scientist: "Arty Aindriu",
    time: "2019-09-03T14:09:38Z"
  }
];
